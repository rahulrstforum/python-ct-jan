# # # # # # product = [
# # # # # #     "iPhone 15",
# # # # # #     "Apple",
# # # # # #     "Some description....",
# # # # # #     100000,
# # # # # #     500,
# # # # # #     "https://url.com/image.png",
# # # # # # ]

# # # # # # products = [
# # # # # #     [
# # # # # #         "iPhone 15",
# # # # # #         "Apple",
# # # # # #         "Some description....",
# # # # # #         100000,
# # # # # #         500,
# # # # # #         "https://url.com/image.png",
# # # # # #     ],
# # # # # #     [
# # # # # #         "iPhone 15",
# # # # # #         "Apple",
# # # # # #         "Some description....",
# # # # # #         100000,
# # # # # #         500,
# # # # # #         "https://url.com/image.png",
# # # # # #     ],
# # # # # #     [
# # # # # #         "iPhone 15",
# # # # # #         "Apple",
# # # # # #         "Some description....",
# # # # # #         100000,
# # # # # #         500,
# # # # # #         "https://url.com/image.png",
# # # # # #     ],
# # # # # # ]

# # # # # product = [
# # # # #     "iPhone 15",
# # # # #     "Apple",
# # # # #     "Some description....",
# # # # #     100000,
# # # # #     500,
# # # # #     "https://url.com/image.png",
# # # # # ]

# # # # # product2 = {
# # # # #     "name": "iPhone 15",
# # # # #     "price": 100000,
# # # # #     "brand": "Apple",
# # # # #     "description": "Some description...",
# # # # #     "in_stock": 500,
# # # # #     "image_url": "https:///...",
# # # # # }

# # # # playlist = [
# # # # {
# # # #     "title": "Some name",
# # # #     "album": "Some album",
# # # #     "artists": ["Artist 1", "Artist 2"],
# # # #     "track_length": 3.49,
# # # # },
# # # #     {
# # # #         "title": "Some name",
# # # #         "album": "Some album",
# # # #         "artists": ["Artist 1", "Artist 2"],
# # # #         "track_length": 3.49,
# # # #     },
# # # #     {
# # # #         "title": "Some name",
# # # #         "album": "Some album",
# # # #         "artists": ["Artist 1", "Artist 2"],
# # # #         "track_length": 3.49,
# # # #     },
# # # # ]


# # # # song = {
# # # #     "title": "Some name",
# # # #     "album": "Some album",
# # # #     "artists": ["Artist 1", "Artist 2"],
# # # #     "track_length": 3.49,
# # # # }


# # # product1 = [
# # #     "iPhone 15",
# # #     "Apple",
# # #     "Some description....",
# # #     100000,
# # #     500,
# # #     "https://url.com/image.png",
# # # ]

# # # product = {
# # #     "name": "iPhone 15",
# # #     "price": 100000,
# # #     "brand": "Apple",
# # #     "description": "Some description...",
# # #     "in_stock": 500,
# # #     "image_url": "https:///...",
# # # }

# # # for key in product.keys():
# # #     print(key, product[key])


# # product = {"first_name": "John", "hello": "world", "test": "Some value"}

# # {"hello": "WORLD" for key in product.keys()}
# # # {"first_name": "JOHN", "hello": "WORLD", "test": "SOME VALUE"}


# # import random

# # print(random.random())


# # def print_lines():
# #     print("hello world")
# #     print("goodbye world")
# #     print("one more thing")


# # print_lines()
# # print_lines()
# # print_lines()

# # DRY

# # print("hello world")
# # print("goodbye world")
# # print("one more thing")

# # print("hello world")
# # print("goodbye world")
# # print("one more thing")

# # print("hello world")
# # print("goodbye world")
# # print("one more thing")


# # import random


# # def flip_coin():
# #     random_num = random.random()

# #     if random_num > 0.5:
# #         print("HEADS")
# #     else:
# #         print("TAILS")


# # flip_coin()
# # flip_coin()
# # flip_coin()


# def greet():
#     print("THIS IS SOMETHING")
#     return "Hello, my name is John"


# result = greet()

# print(f"Value of the result variable: {result}")


# def greet(first_name, last_name, age):
#     return f"Hello my name is {first_name} {last_name} and I am {age} years old"


# print(greet("John", "Doe", 30))
# # print(greet("Jane", "Smith", 35))


# def sum_odd_numbers(numbers):
#     total = 0
#     for num in numbers:
#         if num % 2 != 0:
#             total += num
#     return total


# print(sum_odd_numbers([1, 2, 3, 4, 5, 6]))


# def is_odd_number(number):
#     if number % 2 != 0:
#         return True
#     return False


# print(is_odd_number(1))


# def add(a=0, b=0):
#     return a + b


# print(add(10, 20))

# print(add("hello", "world"))


# def greet(first_name, last_name, age):
#     return f"Hello my name is {first_name} {last_name} and I am {age} years old"


# # print(greet(20, "John", "Doe"))

# print(greet(age=20, last_name="Doe", first_name="John"))


# first_name = "John"


# def say_hello():
#     first_name = "Jack"
#     print("Inside the function body:", first_name)


# say_hello()

# print("Outside the function body:", first_name)


# total = 0


# def dummy():
#     global total
#     total += 1
#     print(total)


# dummy()


# def outer():
#     count = 0

#     def inner():
#         nonlocal count
#         count += 1
#         print(count)

#     inner()


# outer()


def greet(first_name, last_name):
    """Function that accepts two params and returns a string
    @param first_name: str
    @param last_name: str
    @returns str
    """
    return f"Hello my name is {first_name} {last_name}"


# print(greet("John", "Doe"))

# print(greet.__doc__)
# greet

# help(greet)

# import getpass
# import telnetlib

# HOST = "localhost"
# user = input("Enter your remote account: ")
# password = getpass.getpass()

# tn = telnetlib.Telnet(HOST)


# print(password)


# import random

# print(random.randint(10, 20))
# print(random.random())

# from random import randint, random, shuffle

# print(random())
# print(randint(10, 100))


# import random as ran

# from random import randint as ri, random as ran


# def random():
#     print("Something")


# random()
# print(ri(10, 20))
# print(ran())


# import test1
from test1 import greet

greet("Jane", "Doe")
