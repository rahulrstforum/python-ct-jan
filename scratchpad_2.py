# print("Amount of USD you want to convert to INR:")
# amount_in_usd = input("Amount of USD you want to convert to INR: ")

# amount_in_inr = float(amount_in_usd) * 84

# print(f"Amount in INR would be: {amount_in_inr}")

# age = input("Please enter your age: ")
# age = int(age)

# age = int(input("Please enter your age: "))

# if age > 18:
#     print("Welcome to the site")
#     print("This is another line that will only run if the above condition is true")

# print("This will always run")


# age = int(input("Please enter your age: "))

# if age >= 65:
#     print("Drinks are free")
# elif age >= 21:
#     print("You can enter and you can drink.")
# elif age >= 18:
#     print("You may enter, but you cannot drink.")
# else:
#     print("Sorry, you are not allowed here")

# age = int(input("Please enter your age: "))

# if age >= 65:
#     print("Drinks are free")
# elif age >= 21:
#     print("You can enter and you can drink.")
# elif age >= 18:
#     print("You may enter, but you cannot drink.")
# else:
#     print("Sorry, you are not allowed here")

# age = int(input("Please enter your age: "))

# if age >= 18 and age < 21:
#     print("You may enter, but you cannot drink.")
# elif age >= 21 and age < 65:
#     print("You can enter and you can drink.")
# elif age >= 65:
#     print("Drinks are free")
# else:
#     print("Sorry, you are not allowed here")

# logged_in_user = None

# if logged_in_user:
#     print("Welcome to the site")
# else:
#     print("Please login to continue")

# age = input("Please enter your age: ")

# if age:
#     age = int(age)

#     if age >= 18 and age < 21:
#         print("You may enter, but you cannot drink.")
#     elif age >= 21 and age < 65:
#         print("You can enter and you can drink.")
#     elif age >= 65:
#         print("Drinks are free")
#     else:
#         print("Sorry, you are not allowed here")
# else:
#     print("Please enter a value")

# for letter in "hello world":
#     print(letter)

# for num in "hello":
#     print(f"The number is: {num}")


# for num in range(10, 0, -1):
#     print(num)

# for num in range(1, 21):
#     if num == 5 or num == 16:
#         print("FizzBuzz")
#     elif num % 2 == 0:
#         print(f"{num}: Fizz is even")
#     elif num % 2 != 0:
#         print(f"{num}: Fizz is odd")

count = 0
secret_word = input("Please enter your secret word: ")

while secret_word != "helloworld":
    count += 1
    if count == 3:
        print("Sorry attempts over.")
        break
    print("Sorry, that is not the word!")
    secret_word = input("Please enter the word again: ")

# for num in range(10):
#     print(num)

# num = 0
# while num < 10:
#     print(num)
#     num += 1


# secret_word = input("Please enter your secret word: ")

# while secret_word != "helloworld":
#     if secret_word == "quit":
#         break
#     print("Sorry, that is not the word!")
#     secret_word = input("Please enter the word again: ")


# secret_word = input("Please enter your secret word: ")

# while True:
#     if secret_word == "helloworld":
#         break
#     print("Sorry, that is not the word!")
#     secret_word = input("Please enter the word again: ")


# while True:
#     print("hello")
#     break


# users = ["john", "jack", "jill", "julie", "rose"]

# for user in users:
#     print(user)

# index = 0
# while index < 5:
#     print(users[index])
#     index += 1

# nums = [1, 2, 3, 4, 5]
# doubles = [num * 2 for num in nums]

# # for num in nums:
# #     doubles.append(num * 2)

# print(doubles)


# movies = ["Vastav", "Sholay", "Julie", "Dil"]
# uppercased_movies = []

# for movie in movies:
#     uppercased_movies.append(movie.upper())

# print(uppercased_movies)

# movies = ["Vastav", "Sholay", "Julie", "Dil"]
# uppercased_movies = [movie.upper() for movie in movies]

# # for movie in movies:
# #     uppercased_movies.append(movie.upper())

# print(uppercased_movies)
