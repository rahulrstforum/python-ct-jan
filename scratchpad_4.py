# # # # # from random import randint


# # # # # def throw_dice(times=1):
# # # # #     for num in range(times):
# # # # #         value = randint(1, 6)
# # # # #         print("Rolled:", value)


# # # # # throw_dice(10)


# # # # # def add(a=0, b=0, c=0, d=0):
# # # # #     return a + b + c + d


# # # # # print(add(10, 20, 30, 40))


# # # # # def add(*nums):
# # # # #     total = 0
# # # # #     for num in nums:
# # # # #         total += num
# # # # #     return total


# # # # # print(add(10, 20, 123, 213, 123, 123))


# # # # # def dummy(a, b, *others):
# # # # #     print(a)
# # # # #     print(b)
# # # # #     print(others)


# # # # # dummy("hello", 10, 20, 30, True, False)

# # # # # from pprint import pprint


# # # # # def profile(**details):
# # # # #     pprint(details)


# # # # # profile(
# # # # #     email="johndoe@gmail.com",
# # # # #     phone="+91 9876625367",
# # # # # )


# # # # # def dummy(a, b, *args, user_role="moderator", **kwargs):
# # # # #     print(a)
# # # # #     print(b)
# # # # #     print(args)
# # # # #     print(user_role)
# # # # #     print(kwargs)


# # # # # dummy(
# # # # #     "hello",
# # # # #     "world",
# # # # #     "test",
# # # # #     "something",
# # # # #     "else",
# # # # #     user_role="admin",
# # # # #     hello="world",
# # # # #     test="something",
# # # # #     one_more=1000,
# # # # # )


# # # # # def add(*nums):
# # # # #     total = 0
# # # # #     for num in nums:
# # # # #         total += num
# # # # #     return total


# # # # # data = [10, 20, 30, 40, 50, 60]

# # # # # print(add(*data))


# # # # # def print_profile(*details):
# # # # #     print(f"Hello my name is {details[0]} {details[1]}")


# # # # # data = ["Jane", "Doe"]

# # # # # print_profile(*data)

# # # # # total = 0

# # # # # for num in range(1000000000):
# # # # #     total += num

# # # # # print(total)


# # # # # def profile(first_name, last_name, email):
# # # # #     print(f"Hello my name is {first_name} {last_name} and my email is {email}")


# # # # # my_data = {"first_name": "Jane", "last_name": "Doe", "email": "jane.doe@gmail.com"}

# # # # # profile(**my_data)


# # # # # named function
# # # # def add(a, b):
# # # #     return a + b


# # # # def sub(a, b):
# # # #     return a - b


# # # # def mul(a, b):
# # # #     return a * b

# # # # # print(mul(10, 20))
# # # # # functions are values (values are anything you can store in memory)
# # # # a = sub
# # # # print(a(10, 20))
# # # # my_list = [add, sub, mul]

# # # # print(my_list[2](10, 20))


# # # # # add -> named function
# # # # def add(a, b):
# # # #     return a + b


# # # # add2 = lambda a, b: a + b

# # # # print(add2(10, 20))

# # # # def add(a, b):
# # # #     return a + b


# # # # def sub(a, b):
# # # #     return a - b


# # # # def mul(a, b):
# # # #     return a * b


# # # # def math(a, b, add):
# # # #     return add(a, b)


# # # # print(math(10, 20, lambda a, b: a * b))


# # # class User:
# # #     def __init__(self, first_name, last_name, age):
# # #         self.first_name = first_name
# # #         self.last_name = last_name
# # #         self.age = age

# # #     def greet(self):
# # #         return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old."


# # # user3 = User("Jack", "Roe", 40)
# # # print(user3.greet())


# # # user1 = {"first_name": "John", "last_name": "Doe", "age": 30}
# # # user2 = {"first_name": "Jane", "last_name": "Smith", "age": 35}


# # # def greet(user_dict):
# # #     print(
# # #         f"Hello my name is {user_dict['first_name']} {user_dict['last_name']} and I am {user_dict['age']} years old."
# # #     )


# # # # greet(user1)

# # # "hello".upper()
# # # strtoupper("hello")


# # class User:
# #     def __init__(self, fn, ln, email, phone):
# #         self.first_name = fn
# #         self.last_name = ln
# #         self.email = email
# #         self.phone = phone
# #         self.country = "India"

# #     def greet(self):
# #         print(f"Hello below are my details:")
# #         print(f"First Name: {self.first_name}")
# #         print(f"Last Name: {self.last_name}")
# #         print(f"Email: {self.email}")
# #         print(f"Phone: {self.phone}")
# #         print(f"Country: {self.country}")


# # user1 = User("Sachin", "Kohli", "sachin@gmail.com", "+918128323232")
# # user2 = User("Virat", "Tendulkar", "virat@gmail.com", "+918128323232")

# # users_list = [user1, user2]

# # # user1.first_name = "Rohit"
# # # print(user1.first_name)

# # # user1.greet()
# # # user2.greet()

# # for user in enumerate(users_list):
# #     print("User no: ", user[0] + 1)
# #     print(user[1].greet())
# #     # print("---------------")


# def print_in_color(color_name, text):
#     colors = ("red", "yellow", "blue", "green", "orange", "black", "white")

#     if color_name not in colors:
#         raise Exception("ERROR, not a supported color.")

#     print(f"{text} in {color_name}")


# # try:
# #     print_in_color("maroon", "hello world")
# # except:
# #     print("Something went wrong")

# try:
#     print_in_color("maroon", "hello world")
# except Exception as err:
#     print(err)
#     print("Something went wrong")


# print("This code will run after printing the above line.".upper())
