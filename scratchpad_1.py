# # # This is a comment
# # # print("hello world")

# # # print("hello world!")  # This is something

# # high_score = 100
# # # result = high_score + 10
# # # print(result)

# # print(high_score + 10)

# # high_score = 100
# # high_score = "hello"

# # DUCK TYPING

# # score = 100
# # something(score / 2)

# # score: int = 10
# # score = "hello"

# # hello = "😙😚😋sajkdkjashd#$%%#$%$%^$%"
# # print(hello)

# first_name = "john"
# # first_name = ['j', 'o', 'h', 'n']
# # first_name = {0: 'j', 1: 'o', 2: 'h', 3: 'n'}

# message = "Hello, world, this is something."
# message.find("Hello").upper()

age = 25
print("Age:", age)
