contacts = []


def add_contact():
    name = input("Enter the name of the contact: ")
    phone = input("Enter the phone number of the contact: ")
    email = input("Enter the email address of the contact: ")

    contact = {"name": name, "phone": phone, "email": email}
    contacts.append(contact)

    print("Contact added successfully")


def view_contacts():
    if not contacts:
        print("No contacts found")
    else:
        print("Your contacts:")
        for contact in contacts:
            print(f"Name: {contact['name']}")
            print(f"Phone: {contact['phone']}")
            print(f"Email: {contact['email']}")
            print("----------------------------")


while True:
    print("Contact Management")
    print("1. Add Contact")
    print("2: View Contact")
    print("3: Exit")

    choice = input("Enter your choice: ")

    if choice == "1":
        add_contact()
    elif choice == "2":
        view_contacts()
    elif choice == "3":
        print("Thank you for using the contact manager.")
        break
    else:
        print("Invalid choice. Please try again")
